-----------------------------------------------------------------------------------------------
-- Client Lua Script for PerbHealthShieldBar
-- Copyright (c) NCsoft. All rights reserved
-----------------------------------------------------------------------------------------------
require "Window"
require "Apollo"
require "GameLib"
require "Spell"
require "Unit"
require "Item"

local PerbHealthShieldBar = {}

function PerbHealthShieldBar:new(o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self
    return o
end

function PerbHealthShieldBar:Init()
    Apollo.RegisterAddon(self)
end

local knEvadeResource = 7 -- the resource hooked to dodges (TODO replace with enum)

local eEnduranceFlash =
{
	EnduranceFlashZero = 1,
	EnduranceFlashOne = 2,
	EnduranceFlashTwo = 3,
	EnduranceFlashThree = 4,
}

function PerbHealthShieldBar:OnLoad()
	self.xmlDoc = XmlDoc.CreateFromFile("PerbHealthShieldBar.xml")
	self.xmlDoc:RegisterCallback("OnDocumentReady", self)
end

function PerbHealthShieldBar:OnDocumentReady()	
	if self.xmlDoc == nil then return end
	
	Apollo.LoadSprites("BorderSprite.xml", "Perb")
	Apollo.RegisterEventHandler("RefreshPerbHealthShieldBar", "OnFrameUpdate", self)
	Apollo.RegisterEventHandler("VarChange_FrameCount", "OnFrameUpdate", self)
	Apollo.RegisterTimerHandler("EnduranceDisplayTimer", "OnEnduranceDisplayTimer", self)

    self.wndMain = Apollo.LoadForm(self.xmlDoc, "DashFrame", "FixedHudStratum", self)
end

function PerbHealthShieldBar:OnFrameUpdate()
	local unitPlayer = GameLib.GetPlayerUnit()
	if unitPlayer == nil then return end

	local nEvadeCurr = unitPlayer:GetResource(knEvadeResource)
	local nEvadeMax = unitPlayer:GetMaxResource(knEvadeResource)
	
	if nEvadeCurr ~= nEvadeMax and not unitPlayer:IsDead() then
		self.wndMain:Show(true, true)
	else
		self.wndMain:Show(false, true)
	end

	self:UpdateEvades(nEvadeCurr, nEvadeMax)

	if unitPlayer:IsInVehicle() then self.wndMain:Show(false, true) end
end

function PerbHealthShieldBar:UpdateEvades(nEvadeValue, nEvadeMax)
	local nMaxTick = math.floor(nEvadeMax/100)
	local nMaxState = eEnduranceFlash.EnduranceFlashTwo

	if nMaxTick == 3 then
		nMaxState = eEnduranceFlash.EnduranceFlashThree
	end
	
	local nTickValue = nEvadeValue % 100 == 0 and 100 or nEvadeValue % 100
	self.wndMain:FindChild("DashCharge1"):SetMax(100)
	self.wndMain:FindChild("DashCharge2"):SetMax(100)

	if nEvadeValue >= nEvadeMax then -- all full
		self.wndMain:FindChild("DashCharge1"):SetProgress(100)
		self.wndMain:FindChild("DashCharge1"):SetBarColor("xkcdBoringGreen")
		self.wndMain:FindChild("DashCharge2"):SetProgress(100)
		self.wndMain:FindChild("DashCharge2"):SetBarColor("xkcdBoringGreen")
		
		if self.nEnduranceState ~= nMaxState then
			self.nEnduranceState = nMaxState
		end
	elseif math.floor(nEvadeValue/100) < 1 then -- none ready
		self.wndMain:FindChild("DashCharge1"):SetProgress(0)
		self.wndMain:FindChild("DashCharge2"):SetProgress(nTickValue)
		self.wndMain:FindChild("DashCharge2"):SetBarColor("xkcdJungleGreen")

		if self.nEnduranceState ~= eEnduranceFlash.EnduranceFlashZero then
			self.nEnduranceState = eEnduranceFlash.EnduranceFlashZero
		end
	else -- one ready, one filling
		if nMaxState == eEnduranceFlash.EnduranceFlashThree then
			if nEvadeValue >= 200 and nEvadeValue < 300 then
				self.wndMain:FindChild("DashCharge1"):SetProgress(nTickValue)
				self.wndMain:FindChild("DashCharge1"):SetBarColor("xkcdJungleGreen")
				self.wndMain:FindChild("DashCharge2"):SetProgress(100)
			elseif nEvadeValue >= 100 and nEvadeValue < 200 then
				self.wndMain:FindChild("DashCharge1"):SetProgress(nTickValue)
				self.wndMain:FindChild("DashCharge1"):SetBarColor("xkcdJungleGreen")
				self.wndMain:FindChild("DashCharge2"):SetProgress(100)
				self.wndMain:FindChild("DashCharge2"):SetBarColor("xkcdBoringGreen")
			end
		else
			self.wndMain:FindChild("DashCharge1"):SetProgress(nTickValue)
			self.wndMain:FindChild("DashCharge1"):SetBarColor("xkcdJungleGreen")
			self.wndMain:FindChild("DashCharge2"):SetProgress(100)
			self.wndMain:FindChild("DashCharge2"):SetBarColor("xkcdBoringGreen")
		end
	end

	self.nLastEnduranceValue = nEvadeValue
end

local PerbHealthShieldBarInst = PerbHealthShieldBar:new()
PerbHealthShieldBarInst:Init()
